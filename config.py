import json5

# pulled directly from yippee :3
# https://gitlab.com/ACBob/yippee


class YippeeConfig:
	"""
	A simple helper class for interacting with the multi-layered config file.

	Example usage:
	```
	config = YippeeConfig()
	config.load('config.json5')

	# Access a setting value
	api_key = config.get('api', 'key', default='my_default_key')
	```
	"""

	def __init__(self):
		# Holds the config dict
		self._conf = {}

	def load(self, path: str) -> bool:
		"""
		Loads a JSON5 file from the given path and stores it in the config object.

		:param path: The path to the JSON5 file to load.
		:return: True if the file was loaded successfully, False otherwise.
		"""
		with open(path, "r") as f:
			self._conf = json5.load(f)

		return True
	
	def save(self, path: str) -> bool:
		"""
		Saves the current configuration to a JSON5 file at the specified path.

		:param path: The path to save the JSON5 file.
		:return: True if the file was saved successfully, False otherwise.
		"""
		with open(path, "w") as f:
			json5.dump(self._conf, f, indent=2)
		
		return True

	def get(self, *keys, default=None):
		"""
		Returns the value under the specified path from the loaded config file, or the default value if the path does not exist.

		:param keys: A sequence of keys representing the path to the desired value.
		:param default: The default value to return if the path does not exist (default: None).
		:return: The value under the specified path, or the default value.
		"""
		cur = self._conf

		# print(f"GRABBING {'/'.join(keys)}")

		for key in keys:
			try:
				cur = cur[key]
			except (KeyError, TypeError):
				# print("failed to slurp", key)
				return default

		return cur
	
	def size(self, *keys):
		return len(self.get(*keys, default=[]))
	
	def keys(self, *keys):
		return self.get(*keys,default={}).keys()
	
	def has(self, *keys):
		return not self.get(*keys) is None

	def set(self, *keys, value):
		"""
		Sets the value under the specified path in the loaded config file.

		If any part of the path does not exist, it will be created.

		:param keys: A sequence of keys representing the path to the desired value.
		:param value: The value to set at the specified path.
		"""
		cur = self._conf

		for key in keys[:-1]:
			cur = cur.setdefault(key, {})

		cur[keys[-1]] = value

	def pop(self, *keys):
		"""
		Deletes the key at the specified path from the loaded config file.
		Only the last key is deleted.

		If any part of the path does not exist, the function does nothing.

		:param keys: A sequence of keys representing the path to the key to be deleted.
		"""
		cur = self._conf

		for key in keys[:-1]:
			cur = cur.get(key, {})
			if not isinstance(cur, dict):
				return  # Stop if any part of the path is not a dictionary

		last_key = keys[-1]
		if last_key in cur:
			return cur.pop(last_key)
		
		return None

