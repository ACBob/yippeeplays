from abc import ABC, abstractmethod
import sys # for detecting if we can use win32 api raw
from logging import Logger
from time import time

import cemuhook

ENGINES = {}

# A place to pung input to
class OutputEngine(ABC):
	def __init__(self, logger: Logger):
		self.logger = logger
	
	# Press the button on the device
	@abstractmethod
	def press(self, name: str, time: float) -> None:
		pass
	
	@abstractmethod
	def poll(self) -> None: pass
	
	@staticmethod
	@abstractmethod
	def get_valid_pressables() -> list[str]: pass

# any output engine that is keyboard bound
class KeyboardOutputEngine(OutputEngine):
	# NOTE: this maps to win32 keycodes!
	KEYS = {
		"cancel": 0x03,
		"backspace": 0x08,
		"tab": 0x09,
		"enter": 0x0D,
		"shift": 0x10,
		"ctrl": 0x11,
		"alt": 0x12,
		"capslock": 0x14,
		"esc": 0x1B,
		"space": 0x20,
		"pgup": 0x21,
		"pgdown": 0x22,
		"end": 0x23,
		"home": 0x24,
		"leftarrow": 0x26,
		"uparrow": 0x26,
		"rightarrow": 0x27,
		"downarrow": 0x28,
		"select": 0x29,
		"print": 0x2A,
		"execute": 0x2B,
		"printscreen": 0x2C,
		"insert": 0x2D,
		"delete": 0x2E,
		"help": 0x2F,
		"num0": 0x30,
		"num1": 0x31,
		"num2": 0x32,
		"num3": 0x33,
		"num4": 0x34,
		"num5": 0x35,
		"num6": 0x36,
		"num7": 0x37,
		"num8": 0x38,
		"num9": 0x39,
		"a": 0x41,
		"b": 0x42,
		"c": 0x43,
		"d": 0x44,
		"e": 0x45,
		"f": 0x46,
		"g": 0x47,
		"h": 0x48,
		"i": 0x49,
		"j": 0x4A,
		"k": 0x4B,
		"l": 0x4C,
		"m": 0x4D,
		"n": 0x4E,
		"o": 0x4F,
		"p": 0x50,
		"q": 0x51,
		"r": 0x52,
		"s": 0x53,
		"t": 0x54,
		"u": 0x55,
		"v": 0x56,
		"w": 0x57,
		"x": 0x58,
		"y": 0x59,
		"z": 0x5A,
		"leftwin": 0x5B,
		"rightwin": 0x5C,
		"apps": 0x5D,
		"sleep": 0x5F,
		"numpad0": 0x60,
		"numpad1": 0x61,
		"numpad3": 0x63,
		"numpad4": 0x64,
		"numpad5": 0x65,
		"numpad6": 0x66,
		"numpad7": 0x67,
		"numpad8": 0x68,
		"numpad9": 0x69,
		"multiply": 0x6A,
		"add": 0x6B,
		"seperator": 0x6C,
		"subtract": 0x6D,
		"decimal": 0x6E,
		"divide": 0x6F,
		"F1": 0x70,
		"F2": 0x71,
		"F3": 0x72,
		"F4": 0x73,
		"F5": 0x74,
		"F6": 0x75,
		"F7": 0x76,
		"F8": 0x77,
		"F9": 0x78,
		"F10": 0x79,
		"F11": 0x7A,
		"F12": 0x7B,
		"F13": 0x7C,
		"F14": 0x7D,
		"F15": 0x7E,
		"F16": 0x7F,
		"F17": 0x80,
		"F19": 0x82,
		"F20": 0x83,
		"F21": 0x84,
		"F22": 0x85,
		"F23": 0x86,
		"F24": 0x87,
		"numlock": 0x90,
		"scrolllock": 0x91,
		"leftshift": 0xA0,
		"rightshift": 0xA1,
		"leftctrl": 0xA2,
		"rightctrl": 0xA3,
		"leftmenu": 0xA4,
		"rightmenu": 0xA5,
		"browserback": 0xA6,
		"browserforward": 0xA7,
		"browserrefresh": 0xA8,
		"browserstop": 0xA9,
		"browserfavories": 0xAB,
		"browserhome": 0xAC,
		"volumemute": 0xAD,
		"volumedown": 0xAE,
		"volumeup": 0xAF,
		"nexttrack": 0xB0,
		"prevoustrack": 0xB1,
		"stopmedia": 0xB2,
		"playpause": 0xB3,
		"launchmail": 0xB4,
		"selectmedia": 0xB5,
		"launchapp1": 0xB6,
		"launchapp2": 0xB7,
		"semicolon": 0xBA,
		"equals": 0xBB,
		"comma": 0xBC,
		"dash": 0xBD,
		"period": 0xBE,
		"slash": 0xBF,
		"accent": 0xC0,
		"openingsquarebracket": 0xDB,
		"backslash": 0xDC,
		"closingsquarebracket": 0xDD,
		"quote": 0xDE,
		"play": 0xFA,
		"zoom": 0xFB,
		"PA1": 0xFD,
		"clear": 0xFE,
	}
	
	def __init__(self, logger):
		super().__init__(logger)
	
	@staticmethod
	def get_valid_pressables(): return KeyboardOutputEngine.KEYS.keys()
	
	# generally these don't need polling
	def poll(self): return

class PyAutoGuiEngine(KeyboardOutputEngine):
	import pyautogui
	
	def press(self, name: str, time: float) -> None:
		# self.pyautogui.press(name)
		# https://stackoverflow.com/a/72222498
		with self.pyautogui.hold(name):
			self.pyautogui.sleep(time)

ENGINES["PyAutoGui"] = PyAutoGuiEngine

class CemuHookEngine(OutputEngine):
	def __init__(self, logger):
		super().__init__(logger)
		
		# TODO: configurable ip / port
		self.server = cemuhook.CemuHookServer(logger.getChild("Cemu.Server"))
		self.server.start()
		
		self.presses = {}

		self.last_poll = time()
	
	@staticmethod
	def get_valid_pressables(): 
		return cemuhook.FalseController.buttons() + ["randomise"] + CemuHookEngine.get_axis()
	
	@staticmethod
	def get_axis():
		return [
			# push in a direction
			"LeftX+", "LeftX-",
			"LeftY+", "LeftY-",
			"RightX+", "RightX-",
			"RightY+", "RightY-",
		]
	
	def axis(self, name: str, time: float) -> None:
		sign = -1 if name[-1] == "-" else +1 # - / +
		axis = name[-2] # X / Y

		if name.startswith("Left"):
			if axis == "X": self.server.controller.left_joy.x = time * sign
			elif axis == "Y": self.server.controller.left_joy.y = time * sign
		elif name.startswith("Left"):
			if axis == "X": self.server.controller.right_joy.x = time * sign
			elif axis == "Y": self.server.controller.right_joy.y = time * sign

	
	def press(self, name: str, hold: float) -> None:
		# TODO: This effectively stucks the controller
		if name == "randomise":
			self.server.controller.randomise()
		elif name in CemuHookEngine.get_axis():
			self.axis(name, hold)
		else:
			self.server.controller.press(name)
			self.presses[name] = time() + hold
	
	def poll(self):
		self.server.poll()
		
		delta = time() - self.last_poll

		self.server.controller.left_joy.bring_to_rest(delta)
		self.server.controller.right_joy.bring_to_rest(delta)

		for press in list(self.presses.keys()):
			# press things for time
			if time() > self.presses[press]:
				self.server.controller.unpress(press)
				del self.presses[press]
		
		self.last_poll = time()

ENGINES["CemuHook"] = CemuHookEngine
