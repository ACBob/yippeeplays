from time import time
from binascii import crc32
from random import random, choice
import struct
import threading
from logging import Logger
from math import sqrt
import sys

# helper wrapper for packet data
class CemuPacket(list):
	Types = dict(
		version = bytes([0x00, 0x00, 0x10, 0x00]),
		ports = bytes([0x01, 0x00, 0x10, 0x00]),
		data = bytes([0x02, 0x00, 0x10, 0x00]),
	)
	
	def __init__(self, type: str, data: list):
		self.extend([
			0x44, 0x53, 0x55, 0x53,  # DSUS,
			0xE9, 0x03,  # protocol version (1001),
		])
		
		# data length
		self.extend(bytes(struct.pack('<H', len(data) + 4)))

		
		self.extend([
			0x00, 0x00, 0x00, 0x00, # Padding for CRC32
			0xFF, 0xFF, 0xFF, 0xFF, # Server ID
		])
		
		self.extend(CemuPacket.Types[type])
		
		self.extend(data)
		
		# Calculate CRC32
		crc = crc32(bytes(self)) & 0xFFFFFFFF
		self[8:12] = bytes(struct.pack('<I', crc))

# helper for clients
class CemuClient:
	def __init__(self):
		self.ping()
	
	def ping(self):
		self.time = time()
	
	@property
	def timed_out(self):
		return time() - self.time > 5

# A fake wii classic pro controller
# (i mean, it doesn't matter what we model it as I'm just a nerd)
class FalseController:
	# axis of controller, -1 -> +1
	class Axis:
		def __init__(self, x: float = 0, y: float = 0):
			self.x = x
			self.y = y
		
		def randomise(self):
			self.x = random() * 2 - 1
			self.y = random() * 2 - 1
		
		# solution is similar to godot's vector2.move_toward
		def bring_to_rest(self, delta):
			dx = 0 - self.x
			dy = 0 - self.y
			l = sqrt(dx * dx + dy * dy)

			if l <= delta or l < sys.float_info.epsilon:
				self.x = 0
				self.y = 0
			else:
				self.x += dx / l * delta
				self.y += dy / l * delta
		
		def bytify(self) -> tuple[int, int]:
			clamp = lambda x,l,h: min(h, max(l, x))
			return (
				round(clamp(self.x + 1 / 2, 0, 1) * 0xFF), # JOY X
				round(clamp(self.y + 1 / 2, 0, 1) * 0xFF), # JOY Y
			)
	
	# battery (00 - n/a, 0x01 - dying, 0x02 - low, 0x03 - med, 0x04 - hi, 0x05 - full, 0xEE - charging, 0xEF - charged)
	BATTERY_NONE = 0x00
	BATTERY_DYING = 0x01
	BATTERY_LOW = 0x02
	BATTERY_MED = 0x03
	BATTERY_HIGH = 0x04
	BATTERY_FULL = 0x05
	BATTERY_CHARGING = 0xEE
	BATTERY_CHARGED = 0xEF
	
	@staticmethod
	def buttons():
		return [
			"A", "B", "X", "Y",
			"Home",
			"Start", "Select",
			"R1", "L1",
			"ZR", "ZL",
			"R3", "L3",
			"PadUp", "PadDown", "PadLeft", "PadRight",
		]
	
	def set_state(self, button: str, state: bool):
		match button:
			case "A": self.button_a = state
			case "B": self.button_b = state
			case "X": self.button_x = state
			case "Y": self.button_y = state

			case "PadUp": self.dpad_up = state
			case "PadDown": self.dpad_down = state
			case "PadLeft": self.dpad_left = state
			case "PadRight": self.dpad_right = state
			
			case "Home": self.button_home = state
			case "Start": self.button_start = state
			case "Select": self.button_select = state
			
			case "ZL": self.button_back_left = state
			case "ZR": self.button_back_right = state
			
			case "L1": self.button_shoulder_left = state
			case "R1": self.button_shoulder_right = state

			case "L3": self.button_left_joy = state
			case "R3": self.button_right_joy = state
	
	def press(self, button: str): self.set_state(button, True)
	def unpress(self, button: str): self.set_state(button, False)
	
	def __init__(self):
		self.battery_life = FalseController.BATTERY_DYING
		
		# ABXY
		self.button_a = False
		self.button_b = False
		self.button_x = False
		self.button_y = False

		self.dpad_up = False
		self.dpad_down = False
		self.dpad_left = False
		self.dpad_right = False
		
		self.button_home = False
		
		self.button_shoulder_left = False # L1
		self.button_shoulder_right = False # R1
		self.button_back_left = False # L2 / ZL
		self.button_back_right = False # R2 / ZR
		
		self.button_left_joy = False # L3
		self.button_right_joy = False # R3
		
		self.button_start = False
		self.button_select = False
		
		# axis
		# dpad on wii controller modelled is digital
		# self.dpad = FalseController.Axis()
		self.left_joy = FalseController.Axis()
		self.right_joy = FalseController.Axis()
	
	def randomise(self):
		self.battery = choice([
			FalseController.BATTERY_CHARGING, FalseController.BATTERY_CHARGED,
			
			FalseController.BATTERY_FULL,
			FalseController.BATTERY_HIGH,
			FalseController.BATTERY_MED,
			FalseController.BATTERY_LOW,
			FalseController.BATTERY_DYING,
		])
		self.battery_life = FalseController.BATTERY_DYING
		
		# ABXY
		self.button_a = random() > 0.5
		self.button_b = random() > 0.5
		self.button_x = random() > 0.5
		self.button_y = random() > 0.5

		self.dpad_up = random() > 0.5
		self.dpad_down = random() > 0.5
		self.dpad_left = random() > 0.5
		self.dpad_right = random() > 0.5
		
		self.button_home = random() > 0.5
		
		self.button_shoulder_left = random() > 0.5 # L1
		self.button_shoulder_right = random() > 0.5 # R1
		self.button_back_left = random() > 0.5 # L2 / ZL
		self.button_back_right = random() > 0.5 # R2 / ZR
		
		self.button_left_joy = random() > 0.5 # L3
		self.button_right_joy = random() > 0.5 # R3
		
		self.button_start = random() > 0.5
		self.button_select = random() > 0.5
		
		self.left_joy.randomise()
		self.right_joy.randomise()
	
	def bitmask(self) -> tuple[int, int]:
		first_half_o_buttons = 0x00
		second_half_o_buttons = 0x00
		
		# Comments are both the PS4 binding & intuition
		# NOTE: ORDER IS IMPORTANT!
		first_half_o_buttons |= (1 if self.button_start else 0) << 0 # SHARE / START
		first_half_o_buttons |= (1 if self.button_left_joy else 0) << 1 # L3
		first_half_o_buttons |= (1 if self.button_right_joy else 0) << 2 # R3
		first_half_o_buttons |= (1 if self.button_select else 0) << 3 # OPTIONS / SELECT
		
		first_half_o_buttons |= (1 if self.dpad_up else 0) << 4 # DPAD UP
		first_half_o_buttons |= (1 if self.dpad_right else 0) << 5 # DPAD RIGHT
		first_half_o_buttons |= (1 if self.dpad_down else 0) << 6 # DPAD DOWN
		first_half_o_buttons |= (1 if self.dpad_left else 0) << 7 # DPAD LEFT
		
		second_half_o_buttons |= (1 if self.button_back_left else 0) << 0 # L2 / ZL
		second_half_o_buttons |= (1 if self.button_back_right else 0) << 1 # R2 / ZR
		second_half_o_buttons |= (1 if self.button_shoulder_left else 0) << 2 # L1
		second_half_o_buttons |= (1 if self.button_shoulder_right else 0) << 3 # R1
		
		second_half_o_buttons |= (1 if self.button_x else 0) << 4 # TRIANGLE / NORTH
		second_half_o_buttons |= (1 if self.button_a else 0) << 5 # CIRCLE / EAST
		second_half_o_buttons |= (1 if self.button_b else 0) << 6 # CROSS / SOUTH
		second_half_o_buttons |= (1 if self.button_y else 0) << 7 # SQUARE / WEST
		
		return (first_half_o_buttons, second_half_o_buttons)

# Impl notes; pretend to be some controller (Wii Pro?), then communicate
# the controller's status over the UDP CemuHook protocol
# incomming inputs then just report the status of our pseudo controller
class CemuHookServer:
	# https://v1993.github.io/cemuhook-protocol/
	# &
	# https://github.com/joaorb64/joycond-cemuhook / https://github.com/TheDrHax/ds4drv-cemuhook
	# Server impl. based on ds4drv-cemuhook; in theory we can supply to multiple clients.
	
	LOCALHOST = "127.0.0.1"
	UDP_PORT = 26761
	
	def __init__(self, logger: Logger):
		import socket
		
		self.server = socket.socket(
			socket.AF_INET, # Internet
			socket.SOCK_DGRAM, # UDP
		)
		
		self.server.bind((self.LOCALHOST, self.UDP_PORT))
		self.clients = {}
		
		self.controller = FalseController()
		
		self.logger = logger
		
		# TODO: what does this do
		self.counter = 0
	
	def start(self):
		self.thread = threading.Thread(target = self._daemon, daemon = True)
		self.thread.start()
	
	def _daemon(self):
		while True:
			self.handle_request(self.server.recvfrom(1024))
	
	def _slot_info(self, index: int = 0) -> list:
		return [
			index,
			0x02, # (0 - not, 1 - reserved?, 2 - connected) connection state
			0x00, # (0 - no, 1 - partial, 2 - full, 3 - unused) gyro support
			0x00, # (0 - n/a, 1 - usb, 2 - bluetooth) connection type
			*[0x00, 0x00, 0x00, 0x00, 0x00, 0x00], # MAC 00:00:00:00:00:00, zero if not applicable
			self.controller.battery_life,
		]
	
	def poll(self) -> None:
		# useless poll
		if (len(self.clients) == 0):
			return None
		
		data = [
			*self._slot_info(0),
			0x01, # active
		]
		
		data.extend(bytes(struct.pack("<I", self.counter)))
		self.counter += 1
		
		data.extend([
			*self.controller.bitmask(),
			(0xFF if self.controller.button_home else 0x00), # HOME BUTTON / PS
			
			0x00, # TRACK PAD / TOUCH - we don't have
			
			# round(self.controller.left_joy.x * 0.5 + 0.5 * 0xFF), # JOY LEFT X
			# round(self.controller.left_joy.y * 0.5 + 0.5 * 0xFF), # JOY LEFT Y
			# round(self.controller.right_joy.x * 0.5 + 0.5 * 0xFF), # JOY RIGHT X
			# round(self.controller.right_joy.y * 0.5 + 0.5 * 0xFF), # JOY RIGHT Y
			*self.controller.left_joy.bytify(),
			*self.controller.right_joy.bytify(),
			
			(0xFF if self.controller.dpad_up else 0x00), # DPAD UP
			(0xFF if self.controller.dpad_right else 0x00), # DPAD RIGHT
			(0xFF if self.controller.dpad_down else 0x00), # DPAD DOWN
			(0xFF if self.controller.dpad_left else 0x00), # DPAD LEFT
			
			(0xFF if self.controller.button_x else 0x00), # TRIANGLE / NORTH
			(0xFF if self.controller.button_a else 0x00), # CIRCLE / EAST
			(0xFF if self.controller.button_b else 0x00), # CROSS / SOUTH
			(0xFF if self.controller.button_y else 0x00), # SQUARE / WEST
			
			(0xFF if self.controller.button_shoulder_left else 0x00), # R1
			(0xFF if self.controller.button_shoulder_right else 0x00), # L1
			
			(0xFF if self.controller.button_back_left else 0x00), # ZR / R2
			(0xFF if self.controller.button_back_right else 0x00), # ZL / L2
		])
		
		# Touch Data
		# Stubbed as we probably won't, but if needed look @ https://github.com/TheDrHax/ds4drv-cemuhook/blob/master/ds4drv/servers/udp.py#L256
		data.extend([
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00
		])
		
		# gyroscope timestamp
		# data.extend(bytes(struct.pack('<Q', int(time() * 10e6))))
		data.extend(bytes(struct.pack('<Q', 0)))
		
		sensors = [
			0, # roll
			0, # yaw
			0, # pitch
			0, # motion y
			0, # motion x
			0, # motion z
		]
		
		for axis in sensors:
			data.extend(bytes(struct.pack("<f", float(axis))))
		
		self.send_poll(CemuPacket('data', data))
	
	def send_poll(self, packet: CemuPacket) -> None:
		for address, reg in self.clients.copy().items():
			if not reg.timed_out:
				# self.logger.info("Client sent poll: {0[0]}:{0[1]}".format(address))
				self.server.sendto(bytes(packet), address)
			else:
				self.logger.info("Client timeout: {0[0]}:{0[1]}".format(address))
				del self.clients[address]
	
	def handle_request(self, request):
		message, addr = request
		
		kind = message[16:20]
		
		self.logger.info(f"Got Message {kind}")
		# match kind:
		# 	case CemuPacket.Types["version"]: return
		# 	case CemuPacket.Types["ports"]: self.handle_ports(message, addr)
		# 	case CemuPacket.Types["data"]: self.handle_data(message, addr)
		# 	case _:
		# 		self.logger.error("Unknown message kind ", kind)
		
		if kind == CemuPacket.Types["version"]: return
		elif kind == CemuPacket.Types["ports"]: self.handle_ports(message, addr)
		elif kind == CemuPacket.Types["data"]: self.handle_data(message, addr)
		else: self.logger.error("Unknown message kind ", kind)
	
	def ports_msg(self):
		return CemuPacket('ports', [ *self._slot_info(0), 0x00 ])
	
	def handle_ports(self, message, addr):
		requests = struct.unpack("<i", message[20:24])[0]
		
		for i in range(requests):
			index = message[24+1]
			if index > 1: continue
			
			self.server.sendto(bytes(self.ports_msg()), addr)
	
	def handle_data(self, message, addr):
		mode = message[20]
		slot = message[21]
		mac = message[22:28]
		
		if addr not in self.clients:
			self.clients[addr] = CemuClient()
			self.logger.info("Client connected: {0[0]}:{0[1]}".format(addr))
		else:
			self.clients[addr].ping()