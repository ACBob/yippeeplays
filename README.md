# Yippee Plays ...
A simple bot that reads twitch chat and uses it to make actions, either as keyboard or a false controller.

## Using
 - Install Python 3.9, yadda, install from requirements.txt.
 - Create a tokens.txt file in the same place as the bot, filled with an oauth code[^1].
 - Run the `yippee-twitch.py` file, configure the bot and then run the bot!

## Huhs?
 - The bot will raise and ignore exceptions about commands not existing. This seems to be a TwitchIO thing that I can't do much about, just ignore them.
 - The PyAutoGUI engine uses, well, pyautogui to create events, so all events will be sent to the active window.
 - The CemuHook engine uses the [CemuHook Motion Provider/DSU](https://v1993.github.io/cemuhook-protocol/) protocol. While originally intended for Dolphin Emulators, it should work with anything that supports it. (though untested; I've only tried Wii.)

[^1]: You can generate that [here](https://twitchapps.com/tmi/). Note that the bot expects to be ran as the streamer, so it will join its' own chat (so generate the oauth for your account).