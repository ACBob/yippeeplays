from abc import ABC, abstractmethod
from logging import Logger
from threading import Event, Thread
import asyncio
import random
import socket
import requests
import re
from time import time

from engines import OutputEngine
from config import YippeeConfig

class InputSource(ABC):
	ready: Event
	dead: Event
	_die: Event
	
	def __init__(self,
			logger: Logger,
			secrets: list,
			engine: OutputEngine,
			config: YippeeConfig
		):
		self.ready = Event() # Flag to set when we're ready
		self.dead = Event() # Flag to set when we're dead
		self._die = Event() # Flag to set when we SHOULD die
	
	@staticmethod
	def roll_chance(chance: float = 1.0):
		return random() < chance
	
	@abstractmethod
	def run(self): pass
	
	@abstractmethod
	def kill(self): pass

# https://www.learndatasci.com/tutorials/how-stream-text-data-twitch-sockets-python/
class TwitchChatSource(InputSource):
	SERVER = "irc.chat.twitch.tv"
	PORT = 6667

	def __init__(self,
			logger: Logger,
			secrets: list,
			engine: OutputEngine,
			config: YippeeConfig
		):
		super().__init__(logger, secrets, engine, config)
		
		oauth = secrets[0].removeprefix("oauth:")
		
		self.engine = engine
		self.config = config
		
		self.logger = logger.getChild("Source.Twitch")
		self.last_trigger = 0
		self.triggers = {} # insert 2016 joke
		
		# Pull some stuff out of the config we've been handed
		self.prefix = config.get("prefix", default="!")
		self.controls = self.config.get("controls", default={})
		self.allow_help = self.config.get("allowhelp", default=True)

		# Go validate with oauth
		validation = requests.get("https://id.twitch.tv/oauth2/validate", headers={"Authorization":f"OAuth {oauth}"})
		validation.raise_for_status()
		data = validation.json()
		logger.info(data)

		self.nickname = data.get("login")

		self.socket = socket.socket()
		self.socket.connect((TwitchChatSource.SERVER, TwitchChatSource.PORT))
		self.socket.send(f"PASS oauth:{oauth}\n".encode('utf-8'))
		self.socket.send(f"NICK {self.nickname}\n".encode('utf-8'))
		self.socket.send(f"JOIN #{self.nickname}\n".encode('utf-8'))

		# Poll every .5 seconds
		self.socket.settimeout(0.5)

		# TODO: this feels brittle
		while True:
			try:
				response = self.socket.recv(2048).decode('utf-8')
				if "End of /NAMES list" in response:
					break # We joined successfully, probably!
			except socket.timeout: continue
			except Excepction as e:
				self.logger.error(f"Socket error: {e}")
				break
		
		self.logger.info("Joined successfully")

		# We should be ready now
		self.ready.set()

	def run(self):
		while not self._die.is_set():
			try: polled = self.socket.recv(2048).decode('utf-8')
			except socket.timeout: continue

			if not polled:
				break
			self.logger.info(f"Polled [{polled}]")

			# We need to respond to pings
			if polled.startswith("PING"):
				self.socket.send("PONG\n".encode('utf-8'))
			elif len(polled) > 0:
				scanned = re.search(":(\w+)\!\w+@\w+\.tmi\.twitch\.tv (\w+) #(\w+) :(.*)", polled)
				if scanned is None:
					self.logger.warn(f"Encountered an unknown command, ignoring it (hopefully fine?)\n{polled}")
					continue
				
				username, kind, channel, message = scanned.groups()
				message = message.strip()
				print(username, kind, channel, message)
				if kind == "PRIVMSG":
					# Cooling down still, bog off
					if time() - self.last_trigger < self.config.get("cooldown"):
						return

					if message.startswith(self.prefix):
						call: str = message[len(self.prefix) :].split(" ")
						command: str = call[0].lower()
						arg: float = float(call[1]) if len(call) > 1 else 0.1

						# Trying to get help..
						# TODO
						# if command in ["help", "?", "commands"] and self.allow_help:
						# 	ctx = await self.get_context(message)
						# 	await ctx.reply("Current Commands: " + ", ".join(self.controls.keys()))
						
						# Trying to run a command..
						if command in self.controls:
							settings = self.controls[command]
							chance = settings.get("chance", 1)
							roll = random.randint(0, 100)
							timing = settings.get("timing", "Press")

							strength = min(arg, self.config.get("max_argument_time", default=5))
							if timing == "Press": strength = 0.1
							elif timing == "Jab": strength = 2

							# this is still cooling off
							cooldown = settings.get("cooldown", 0)
							if command in self.triggers and time() - self.triggers[command] < cooldown:
								return
							
							self.logger.info(f"Rolled a {roll} against {chance} for {command}")
							if roll <= chance:
								self.engine.press(settings["bind"], strength)
								self.last_trigger = time()
								self.triggers[command] = time()
							elif self.config.get("cool_on_failed_roll", False):
								self.triggers[command] = time()

		self.logger.info("Dead :)")
		self.socket.close()
		self.dead.set()
	
	def kill(self):
		self.logger.info("Killing Twitch...")
		self._die.set()