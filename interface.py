from PyQt6.QtWidgets import *
from PyQt6.QtCore import *
from PyQt6.QtGui import QKeySequence, QAction, QDoubleValidator, QIcon
from logging import Logger
import threading

from config import YippeeConfig
import sources
import engines

# lets us smack a combobox inside a table view
class ComboBoxDelegate(QStyledItemDelegate):
	def __init__(self, parent=None, possible_values=[]):
		super().__init__(parent)
		self.possible_values = possible_values
	
	def set_possible_values(self, values=[]):
		self.possible_values = values
	
	def createEditor(self, parent, option, index):
		editor = QComboBox(parent)
		editor.addItems(self.possible_values)
		return editor

	def setEditorData(self, editor, index):
		current_text = index.model().data(index, Qt.ItemDataRole.EditRole)
		combo_box = editor
		combo_box.setCurrentText(current_text)

	def setModelData(self, editor, model, index):
		combo_box = editor
		model.setData(index, combo_box.currentText(), Qt.ItemDataRole.EditRole)

# lets us smack a double combobox inside a table view
class DoubleEditDelegate(QStyledItemDelegate):
	def __init__(self, parent=None, suffix="%", minimum=0, maximum=100, step = 1):
		super().__init__(parent)
		self.suffix = suffix
		self.minimum = minimum
		self.maximum = maximum
		self.step = step
	
	def createEditor(self, parent, option, index):
		editor = QDoubleSpinBox(parent,
			minimum = self.minimum,
			maximum = self.maximum,
			suffix = self.suffix,
			singleStep = self.step
		)
		return editor

	def setEditorData(self, editor, index):
		value = index.model().data(index, Qt.ItemDataRole.EditRole)
		editor.setValue(value)

	def setModelData(self, editor, model, index):
		model.setData(index, editor.value(), Qt.ItemDataRole.EditRole)

class BindTableModel(QAbstractTableModel):
	name_blocked = pyqtSignal(str, name="nameBlocked")

	def __init__(self, parent, config: YippeeConfig):
		super().__init__(parent)
		self.config = config
	
	def rowCount(self, parent=None):
		return self.config.size("controls")
	
	def columnCount(self, parent=None):
		return 5
	
	def flags(self, index):
		if index.column() <= 4:
			return Qt.ItemFlag.ItemIsEditable | Qt.ItemFlag.ItemIsEnabled | Qt.ItemFlag.ItemIsSelectable
		return Qt.ItemFlag.ItemIsEnabled | Qt.ItemFlag.ItemIsSelectable
	
	def setData(self, index, value, role=Qt.ItemDataRole.EditRole):
		if role == Qt.ItemDataRole.EditRole:
			row = index.row()
			col = index.column()
			
			key = list(self.config.keys("controls"))[row]

			match col:
				case 0:
					old_key = key
					new_key = value
					if len(new_key) < 1:
						self.name_blocked.emit(new_key)
					elif old_key != new_key and not self.config.has("controls", new_key):
						self.config.set("controls", new_key, value=self.config.pop("controls", old_key))
						self.dataChanged.emit(index, index)
					elif old_key != new_key:
						self.name_blocked.emit(new_key)
					return True

				case 1:
					self.config.set("controls", key, "bind", value=value)
					self.dataChanged.emit(index, index)
					return True

				case 2:
					self.config.set("controls", key, "chance", value=float(value))
					self.dataChanged.emit(index, index)
					return True
				
				case 3:
					self.config.set("controls", key, "cooldown", value=float(value))
					self.dataChanged.emit(index, index)
					return True
				
				case 4:
					self.config.set("controls", key, "timing", value=value)
					self.dataChanged.emit(index, index)
					return True
	
	def data(self, index, role=Qt.ItemDataRole.DisplayRole):
		row = index.row()
		col = index.column()
		
		if role == Qt.ItemDataRole.DisplayRole or role == Qt.ItemDataRole.EditRole:
			key = list(self.config.get("controls").keys())[row]

			match col:
				case 0: return key
				case 1: return self.config.get("controls", key, "bind")
				case 2:
					if role == Qt.ItemDataRole.DisplayRole:
						return "%.2f%%" % self.config.get("controls", key, "chance", default=100)
					return self.config.get("controls", key, "chance", default=100)
				case 3: return self.config.get("controls", key, "cooldown", default=0.0)
				case 4: return self.config.get("controls", key, "timing", default="Press")
	
	def headerData(self, section, orientation, role):
		if orientation == Qt.Orientation.Horizontal and role == Qt.ItemDataRole.DisplayRole:
			return ["Command", "Bind", "Chance", "Cooldown", "Strength"][section]
		return QVariant()


class BindDialog(QDialog):
	def __init__(self, parent, possible_values):
		super().__init__(parent)

		self.buttons = QDialogButtonBox(
			QDialogButtonBox.StandardButton.Ok |
			QDialogButtonBox.StandardButton.Cancel,
			self
		)

		self.name_input = QLineEdit(self, toolTip="Command Name")
		self.chance_input = QDoubleSpinBox(parent,
			minimum = 0,
			maximum = 100,
			suffix = "%",
			toolTip = "Percent chance for the command to run."
		)

		self.cooldown_input = QDoubleSpinBox(parent,
			minimum = 0,
			maximum = 3600,
			toolTip = "Amount of time to cooldown running this command."
		)

		self.bind_dropdown = QComboBox(self, toolTip="Engine button to press")

		self.strength_dropdown = QComboBox(self, 
			toolTip="Strength of press.\nPress is .1 seconds, Jab is 2 seconds,\nArgument lets chat decide")
		self.strength_dropdown.addItems(["Press", "Jab", "Argument"])

		form = QFormLayout()
		form.addRow("&Name", self.name_input)
		form.addRow("&Bind", self.bind_dropdown)
		form.addRow("&Chance", self.chance_input)
		form.addRow("Cool&down", self.cooldown_input)
		form.addRow("&Strength", self.strength_dropdown)
		
		self.bind_dropdown.addItems(possible_values)
		self.chance_input.setValue(100)

		layout = QVBoxLayout()
		layout.addLayout(form)
		layout.addWidget(self.buttons)
		self.setLayout(layout)

		self.buttons.accepted.connect(self.accept)
		self.buttons.rejected.connect(self.reject)


class MainWindow(QMainWindow):
	logger: Logger
	output_engine_kind: engines.OutputEngine
	input_source_kind: sources.InputSource
	
	def __init__(self, logger: Logger):
		super().__init__(None)
		
		self.logger = logger
		
		# config we use
		self.config = YippeeConfig()
		
		self.main_widget = QWidget()
		self.setCentralWidget(self.main_widget)
		
		self.create_ui()
		self.create_menubar()
		self.setWindowTitle("YippeePlays")
		self.setWindowIcon(QIcon("assets/icon.png"))
		self.resize(700, 600)
		self.setMinimumSize(400, 500)
		
		# Thread we hold the twitch bot on
		self.worker_thread = None
		
		self.output_engine_kind = engines.PyAutoGuiEngine # the engine we use for outputting
		self.output_engine = None # output engine instance
		
		self.input_source_kind = sources.TwitchChatSource # place we use for getting input
		self.input_source = None # input source instance
		
	
	def create_ui(self):
		self.status_bar = QStatusBar(self)
		self.setStatusBar(self.status_bar)
		
		self.status_bar.showMessage("Idle...")
		
		vbox = QVBoxLayout()
		self.main_widget.setLayout(vbox)
		
		self.editing_widgets = QWidget()
		edit_vbox = QVBoxLayout()
		self.editing_widgets.setLayout(edit_vbox)
		vbox.addWidget(self.editing_widgets)
		
		form = QFormLayout(self)
		edit_vbox.addLayout(form)
		
		self.prefix_input = QLineEdit(
			self,
			toolTip="Text that must prepend input to be considered a command",
			textEdited=lambda prefix: self.config.set("prefix", value=prefix),
		)
		
		self.delay_input = QDoubleSpinBox(
			self,
			toolTip="Cooldown between listening to input. First come first serve!",
			minimum=0,
			singleStep=0.25,
			maximum=3600,
			valueChanged=lambda value: self.config.set("cooldown", value=value),
		)

		self.max_twitch_hold = QDoubleSpinBox(
			self,
			toolTip="Longest time twitch can manually hold a button for in \"Argument\" strength",
			minimum=0,
			singleStep=0.25,
			maximum=3600,
			valueChanged=lambda value: self.config.set("max_argument_time", value=value),
		)
		
		form.addRow("&Prefix", self.prefix_input)
		form.addRow("&Cooldown", self.delay_input)
		form.addRow("Max &Argument Time", self.max_twitch_hold)
		
		hbox = QHBoxLayout()
		edit_vbox.addLayout(hbox)
		
		self.binding_table = QTableView()
		self.binding_model = BindTableModel(self, self.config)
		self.binding_table.setModel(self.binding_model)
		
		self.bind_drop_delegate = ComboBoxDelegate(self.binding_table)
		self.binding_table.setItemDelegateForColumn(1, self.bind_drop_delegate)

		self.binding_table.setItemDelegateForColumn(2, DoubleEditDelegate(self, "%", 0, 100))
		self.binding_table.setItemDelegateForColumn(3, DoubleEditDelegate(self, "", 0, 3600, 0.25))
		self.binding_table.setItemDelegateForColumn(4, ComboBoxDelegate(self, ["Press", "Jab", "Argument"]))

		self.binding_model.name_blocked.connect(lambda name: 
			self.warn_empty_control() if len(name) < 1
			else self.warn_dupicate_control(name)
		)
		
		header = self.binding_table.horizontalHeader()
		header.setSectionResizeMode(0, QHeaderView.ResizeMode.Stretch)
		header.setSectionResizeMode(1, QHeaderView.ResizeMode.Stretch)
		header.setSectionResizeMode(2, QHeaderView.ResizeMode.Stretch)
		header.setStretchLastSection(True)
		
		self.engine_dropdown = QComboBox(self, currentTextChanged=self.select_engine)
		for engine in engines.ENGINES:
			self.engine_dropdown.addItem(engine)
		
		self.engine_dropdown.setToolTip("The output engine we use")

		self.burn_check = QCheckBox(
			"Burn on failed roll",
			toolTip="When false, we can immediately try re-rolling a failed command.",
			stateChanged=lambda state: 
				# self.config.set("cool_on_failed_roll", value = (state == Qt.CheckState.Checked))
				self.config.set("burn_failed_rolls", value = state == 2)
			)
		
		form.addRow(self.burn_check)
		
		form.addRow("&Engine", self.engine_dropdown)
		
		hbox.addWidget(self.binding_table)
		
		buttonVBox = QVBoxLayout()
		hbox.addLayout(buttonVBox)
		
		self.add_bind_button = QPushButton(QIcon.fromTheme("list-add"), "", clicked=self.add_bind)
		self.remove_bind_button = QPushButton(QIcon.fromTheme("list-remove"), "", clicked=self.remove_bind)
		
		buttonVBox.addWidget(self.add_bind_button)
		buttonVBox.addWidget(self.remove_bind_button)
		buttonVBox.addStretch()
		
		self.toggle_button = QPushButton("&Start", clicked=self.toggle)
		vbox.addWidget(self.toggle_button)
		# vbox.addWidget(QPushButton("test", clicked=lambda: print(self.binding_model.config._conf)))
	
	def create_menubar(self):
		self.menubar = self.menuBar()
		
		file_menu = self.menubar.addMenu("&File")
		
		open_action = QAction(QIcon.fromTheme("document-open"), "&Open", self, triggered=self.find_open_file)
		open_action.setShortcut(QKeySequence.StandardKey.Open)
		file_menu.addAction(open_action)
		
		save_action = QAction(QIcon.fromTheme("document-save-as"), "Save &As", self, triggered=self.find_save_file)
		save_action.setShortcut(QKeySequence.StandardKey.SaveAs)
		file_menu.addAction(save_action)
		
		file_menu.addSeparator()
		
		quit_action = QAction(QIcon.fromTheme("application-exit"), "&Quit", self, triggered=self.close)
		quit_action.setShortcut(QKeySequence.StandardKey.Quit)
		file_menu.addAction(quit_action)
		
		help_menu = self.menubar.addMenu("&Help")
		
		about_action = QAction(QIcon.fromTheme("help-about"), "&About", self, triggered=self.about)
		help_menu.addAction(about_action)
	
	def select_engine(self, engine:str):
		self.output_engine_kind = engines.ENGINES[engine]
		self.config.set("engine", value=engine)
		
		# print(self.output_engine_kind.get_valid_pressables())
		
		# we want to re-set the dropdowns
		self.bind_drop_delegate.set_possible_values(self.output_engine_kind.get_valid_pressables())
		
	
	def add_bind(self):
		dlg = BindDialog(self, self.output_engine_kind.get_valid_pressables())
		dlg.setWindowTitle("Add Control..")

		while dlg.exec():
			name = dlg.name_input.text()

			if self.config.has("controls", name):
				self.warn_dupicate_control(name)
			elif len(name) < 1:
				self.warn_empty_control()
			else:
				bind = dlg.bind_dropdown.currentText()
				chance = dlg.chance_input.value()
				cooldown = dlg.cooldown_input.value()
				strength = dlg.strength_dropdown.currentText()

				# TODO: bad practice
				self.binding_model.beginResetModel()
				self.config.set("controls", name, value={"bind":bind, "chance": chance, "cooldown": cooldown, "timing": strength})
				self.binding_model.endResetModel()

				break
	
	def remove_bind(self):
		self.binding_model.beginResetModel()
		rows = sorted(set(index.row() for index in self.binding_table.selectedIndexes()))
		keys = list(self.config.keys("controls"))
		for row in rows:
			self.config.pop("controls", keys[row])
		self.binding_model.endResetModel()

	def warn_dupicate_control(self, name):
		return QMessageBox.critical(
			self,
			"Oops!",
			f"There's already a control called <b>{name}</b>.",
			buttons=QMessageBox.StandardButton.Ok,
			defaultButton=QMessageBox.StandardButton.Ok
		)

	def warn_empty_control(self):
		return QMessageBox.critical(
			self,
			"Nuh Uh!",
			f"Can't have a command with an empty name!",
			buttons=QMessageBox.StandardButton.Ok,
			defaultButton=QMessageBox.StandardButton.Ok
		)
	
	def find_open_file(self):
		path = QFileDialog.getOpenFileName(self, "Open", "", "Configurations (*.json5)")[0]
		if path:
			self.load_from_file(path)
	
	def find_save_file(self):
		path = QFileDialog.getSaveFileName(self, "Save", "", "Configurations (*.json5)")[0]
		if path:
			self.config.save(path)
	
	def about(self):
		QMessageBox.about(self, "About YippeePlays",
			"<center>"\
			"<h1>YippeePlays</h1>"\
			"<p>Twitch Bot for chat-based input</p>"\
			"</center>"\
			"<p>Version 2 &quot;givinashit&quot;</p>"\
			"<p>Written by <a href=\"https://acbob.xyz\">ACBob</a> for the"\
				" <a href=\"https://linktr.ee/emeraldquadron\">Emerald Quadron</a></p>"
		)
	
	def load_from_file(self, filepath: str):
		self.binding_model.beginResetModel()
		self.config.load(filepath)
		self.binding_model.endResetModel()
		
		# print(self.config.size("controls"))
		
		self.status_bar.showMessage("Opening...")
		
		self.engine_dropdown.setCurrentText(self.config.get("engine"))
		self.prefix_input.setText(self.config.get("prefix", default = "!"))
		self.delay_input.setValue(self.config.get("cooldown", default = 0.25))
		self.max_twitch_hold.setValue(self.config.get("max_argument_time", default = 5))
		self.burn_check.setChecked(self.config.get("burn_failed_rolls", default=False))
		
		self.status_bar.showMessage("Idle...")
	
	def set_ui_usable(self, usable: bool):
		self.editing_widgets.setEnabled(usable)
	
	def toggle(self):
		# Turn off editing
		self.set_ui_usable(False)
		
		# Running
		if self.worker_thread is not None:
			self.set_ui_usable(True)
			self.toggle_button.setText("&Start")
			self.toggle_button.setStyleSheet("")
			
			self.status_bar.showMessage("Stopping...")
			self.input_source.kill()

			# Update interface
			QApplication.processEvents()
			
			# Wait for death
			while not self.input_source.dead.is_set():
				QApplication.processEvents()
			
			del self.input_source
			self.input_source = None
			
			del self.worker_thread
			self.worker_thread = None
			
			self.status_bar.showMessage("Idle...")
		else:
			self.status_bar.showMessage("Starting...")
			self.toggle_button.setEnabled(False)

			QApplication.processEvents()
			
			self.toggle_button.setText("&STOP YIPPEE")
			self.toggle_button.setStyleSheet("background: #ff0000;color:#000")
			
			# open the secrets file
			secrets = []
			try:
				with open(self.config.get("secrets", default="tokens.txt"), "r") as secrets_file:
					secrets = secrets_file.read().split()
			except FileNotFoundError:
				self.toggle_button.setEnabled(True)
				self.toggle_button.setText("&Start")
				self.toggle_button.setStyleSheet("")

				self.set_ui_usable(True)

				QMessageBox.critical(self, "Drat!",
					"Cannot find twitch OAuth secret token!<br> \
					Make sure a 'tokens.txt' exists in the CWD with your Twitch OAuth2 token.<br> \
					Go <a href='https://twitchapps.com/tmi/'>here</a> to generate one.",
					buttons=QMessageBox.StandardButton.Cancel,
					defaultButton=QMessageBox.StandardButton.Cancel
				)
				self.status_bar.showMessage("Womp...")
				return

			
			self.input_source = self.input_source_kind(
				self.logger,
				secrets,
				self.output_engine_kind(self.logger),
				self.config
			)
			
			self.worker_thread = threading.Thread(target=self.input_source.run)
			self.worker_thread.start()
			
			# wait for the source to be ready..
			while not self.input_source.ready.is_set():
				QApplication.processEvents()
			
			self.status_bar.showMessage("Running...")
			self.toggle_button.setEnabled(True)
