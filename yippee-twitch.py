#!/bin/python
import engines
import interface

from PyQt6.QtWidgets import QApplication
import logging

if __name__ == "__main__":
	import sys
	import tkinter as tk

	logging.basicConfig(stream=sys.stdout, level=logging.INFO)
	logger = logging.getLogger("YippeePlays")
	
	app = QApplication([])
	ui = interface.MainWindow(logger)
	ui.load_from_file("config.json5")
	ui.show()
	
	app.exec()

	# config = config.YippeeConfig()
	# config.load("config.json5")

	# bot = YippeePlaysTwitch(oauth, config)
	# botthread = threading.Thread(target=bot.run, daemon=True)
	# botthread.start()

	# bigred = create_bigredbutton()
	# bigred.mainloop()
	# bot.close()
